import jinja2
import os

from markdown import markdown
import yaml
import json

# for date and time filtering
import datetime
from dateutil.parser import *

# for media copies
from distutils.dir_util import copy_tree

# for TOC extraction
from bs4 import BeautifulSoup

# for custom markdown comment-sections
import re

# for pads imports
import requests


# OPTIONS
# ------------------------------------------------------------------

PAD = 'http://pads.osp.kitchen/p/collaboration-agreement-rewriting'
TEMPLATE_PATH = 'template.html'
HTML_PATH = 'www/'

options = {
    'url': 'https://collaboration.osp.kitchen',
    'title': 'collaboration agreement',
    'description': '', 
    'TOC': True
}


# MARKDOWN EXT
# ------------------------------------------------------------------

# https://python-markdown.github.io/extensions/extra/ 
# (official support for: fenced code block, footnotes)
from markdown.extensions.extra import ExtraExtension

# https://python-markdown.github.io/extensions/toc/
# (official support for TOC)
from markdown.extensions.toc import TocExtension

# https://python-markdown.github.io/extensions/code_hilite/
# (official support for syntax highlighting)
# from markdown.extensions.codehilite import CodeHiliteExtension

# https://github.com/funk1d/markdown-figcap
# (third party for figcaption, allow complexe ones with html in it and custom figure content)
# from markdown_figcap import FigCapExtension

# https://github.com/evidlo/markdown_captions
# (third party for figcaption, simply transform img alt text into caption)
# NOTE: figure still wrapped in <p>
# from markdown_captions import CaptionsExtension

# https://pypi.org/project/markdown3-newtab/
# (third party that make every link into a new tab link)
# NOTE: create a link typology extension
# from markdown3_newtab import NewTabExtension

from yafg import YafgExtension

MARKDOWN = {
    'extensions': [
        YafgExtension(stripTitle=False),
        ExtraExtension(),
        # CodeHiliteExtension(),
        # FigCapExtension(),
        # FootnoteExtension(),
        TocExtension(toc_depth='1-3')
    ],
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'codelight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.footnotes': {},
    }
}


# PARSING
# ------------------------------------------------------------------

def read_file(NAME_PATH):
    txt = ""
    with open(NAME_PATH, 'r') as file:
        txt = file.read()
    return txt

def md_parse(txt):
    md = ""
    md = markdown(txt, 
    extensions=MARKDOWN['extensions'], 
    extension_configs=MARKDOWN['extension_configs'])
    return md


def yml_parse(txt):
    yml = ""
    yml = yaml.safe_load(txt)
    return yml


def split_sections(page):
    """
    Split the markdown content of an article or page into sections
    everytime it encounter a comment of the form <!--[section-name]--> 
    and put those in a new `sections` attribute as a dictonnary.

    The `content` attribute is left intact.
    """

    sections = []

    pattern = re.compile(r"<!--\[((?:(?!\]-->).)*)\]-->((?:(?!<!--\[)[\s\S])+)")

    # find all matches to groups
    matches = pattern.finditer(page['txt'])

    for match in matches:
        # section name
        section_name = match.group(1)
        section_content = match.group(2)
        page[section_name] = section_content
        sections.append(section_name)

    if sections:
        sections_names = ", ".join(sections)
        print("Sections: {s}".format(s=sections_names))


# COLLECTING CONTENT
# ------------------------------------------------------------------

def collect_content():
    content = {}
    pad = requests.get(PAD + '/export/txt')
    content['text'] = md_parse(pad.text)

    # extracting TOC to a 'toc' value
    # if options['TOC']:
    #     soup = BeautifulSoup(content['text'], 'html.parser')
    #     find_toc = soup.find("div", class_="toc")
    #     if find_toc:
    #         toc = find_toc.extract()
    #         content['toc'] = toc
    #         content['text'], = str(soup)
    
    return content


# MAIN
# ------------------------------------------------------------------

def write_html(template, content, path, basename = "index"):
    html = template.render(content = content, options = options, basename = basename)
    with open(path, 'w') as file:
        file.write(html)

if __name__ == '__main__':

    # collect content
    content = collect_content()

    # init jinja environment
    templateLoader = jinja2.FileSystemLoader( searchpath="" )
    env = jinja2.Environment(
        loader=templateLoader
    )

    # getting the template
    template = env.get_template(TEMPLATE_PATH)

    # render template
    write_html(template, content, HTML_PATH + 'index.html')