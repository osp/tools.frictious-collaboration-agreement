a TLDR of the collaboration agreement.
written at the OSP retreat of June 2024, including Gijs de Heij, Ludi Loiseau, Doriane Timmermans, Simon Browne, Vinciane Daheron, Clara Pasteau. It is under CC4R license. 

To generate the page, run:

        python3 make.py

The content is in a pad.

To install requirements, run:

        pip install -r requirements.txt

to upload make an `.env` file containing:

```
SSH_HOST="osp.kitchen"
SSH_LOCATION="/srv/www/kitchen.osp.collaboration"
SSH_USERNAME="username"
SSH_PASSWORD="password"
```

and, subsequently run:

        ./scp-sync.sh

