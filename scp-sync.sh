#!/usr/bin/env bash

# get variable from .env file
if [ -f .env ]
then
  export $(cat .env | xargs)
fi

scp -r ./www/* $SSH_USERNAME@$SSH_HOST:$SSH_LOCATION